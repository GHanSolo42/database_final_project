-- Centralized user, schema, and table set up for grading purposes.
DROP DATABASE IF EXISTS finalproject;
CREATE DATABASE finalproject;
DROP USER IF EXISTS finalproject;
CREATE USER finalproject WITH PASSWORD 'finalproject';
GRANT ALL PRIVILEGES ON DATABASE finalproject to finalproject;
GRANT ALL PRIVILEGES ON DATABASE finalproject to postgres;
ALTER USER finalproject SET search_path = finalschema;

DROP SCHEMA IF EXISTS finalschema CASCADE;
CREATE SCHEMA finalschema;
-- GRANT ALL PRIVILEGES ON SCHEMA finalschema TO finalproject;

DROP TABLE IF EXISTS descriptions CASCADE;
DROP TABLE IF EXISTS apps CASCADE;
DROP TABLE IF EXISTS reviews CASCADE;
DROP TABLE IF EXISTS ratings CASCADE;
DROP TABLE IF EXISTS info CASCADE;

CREATE TABLE descriptions (
    name VARCHAR(255) PRIMARY KEY,
    description VARCHAR(4096)
);
CREATE TABLE apps (
    name VARCHAR(255),
    genre VARCHAR(255),
    PRIMARY KEY (name, genre),
    FOREIGN KEY (name) REFERENCES descriptions (name)
);
CREATE TABLE reviews (
    name VARCHAR(255),
    user_review VARCHAR (4096),
    FOREIGN KEY (name) REFERENCES descriptions (name)
);
CREATE TABLE ratings (
    name VARCHAR(255),
    rating FLOAT,
    review_count BIGINT,
    content_rating VARCHAR(63),
    FOREIGN KEY (name) REFERENCES descriptions (name)
);
CREATE TABLE info (
    name VARCHAR(255),
    version VARCHAR(63),
    size BIGINT,
    price MONEY,
    downloads BIGINT,
    total_ratings BIGINT,
    FOREIGN KEY (name) REFERENCES descriptions (name)
);