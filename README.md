**Note to Professor:** We would like a regrade of our schema. 
- The extra file, schema_regrade.sql, is our compilation of load_data.py and setup_final_project.sql for convenience
- The most current load_data.py and setup_final_project.sql are located in the Gitlab repo

Gitlab Link:
https://gitlab.com/GHanSolo42/database_final_project

This is a Python 3 application

Install Requirements

```pip3 install -r requirements.txt```

Load Database:
- Instructions found in data_loading folder
- This folder can be re-downloaded from Gitlab

Run

```python3.6 main.py```

Shell Workflow

```
Successfully Connected
Welcome to the shell. Type help or ? to list available commands
shell >> top_ten_genres 
action                     maps_and_navigation
action_and_adventure       medical
adventure                  music
arcade                     music_and_audio
art_and_design             music_and_video
auto_and_vehicles          navigation
beauty                     news
board                      news_and_magazines
book                       parenting
books_and_reference        personalization
brain_games                photo_and_video
business                   photography
card                       pretend_play
casino                     productivity
casual                     puzzle
catalogs                   racing
comics                     reference
communication              role_playing
creativity                 shopping
dating                     simulation
education                  social
educational                social_networking
entertainment              sports
events                     strategy
family                     tools
finance                    travel
food_and_drink             travel_and_local
game                       trivia
games                      utilities
health_and_fitness         video_players
house_and_home             video_players_and_editors
libraries_and_demo         weather
lifestyle                  word
shell >> top_ten_genres a
action                adventure             art_and_design
action_and_adventure  arcade                auto_and_vehicles
shell >> top_ten_genres a
action                adventure             art_and_design
action_and_adventure  arcade                auto_and_vehicles
shell >> top_ten_genres action
Top Ten Apps in action
    1. Temple Run 2
    2. Talking Tom Gold Run
    3. Sniper 3D Gun Shooter: Free Shooting Games - FPS
    4. Garena Free Fire
    5. Crossy Road
    6. Mobile Legends: Bang Bang
    7. Banana Kong
    8. Modern Combat 5: eSports FPS
    9. Helix Jump
    10. Bus Rush: Subway Edition
shell >> 
```

# Features
- Given an app name, list all the info
- Given a category, list the average rating for that category
- List all apps with a rating > __ 
- Given a category, list all the apps
- Top ten most popular apps per genre/in general
- Top ten apps within a certain price range
