import csv
import psycopg2

def printProgressBar (iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, fill = '█'):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end = '\r')
    # Print New Line on Complete
    if iteration == total: 
        print()

def main():
    conn = psycopg2.connect("host=localhost dbname=finalproject user=finalproject password=finalproject")
    conn.autocommit = True
    cur = conn.cursor()
    cur.execute('DROP SCHEMA IF EXISTS finalschema CASCADE')
    cur.execute('CREATE SCHEMA finalschema')
    # cur.execute('GRANT ALL PRIVILEGES ON SCHEMA finalschema TO finalproject')

    cur.execute('DROP TABLE IF EXISTS descriptions CASCADE')
    cur.execute('DROP TABLE IF EXISTS apps CASCADE')
    cur.execute('DROP TABLE IF EXISTS reviews CASCADE')
    cur.execute('DROP TABLE IF EXISTS ratings CASCADE')
    cur.execute('DROP TABLE IF EXISTS info CASCADE')

    cur.execute("""
        CREATE TABLE descriptions (
            name VARCHAR(255) PRIMARY KEY,
            description VARCHAR(4096)
        )
    """)

    cur.execute("""
        CREATE TABLE apps (
            name VARCHAR(255),
            genre VARCHAR(255),
            PRIMARY KEY (name, genre),
            FOREIGN KEY (name) REFERENCES descriptions (name)
        )
    """)
    
    cur.execute("""
        CREATE TABLE reviews (
            name VARCHAR(255),
            user_review VARCHAR (4096),
            FOREIGN KEY (name) REFERENCES descriptions (name)
        )  
    """)

    cur.execute("""
        CREATE TABLE ratings (
            name VARCHAR(255),
            rating FLOAT,
            review_count BIGINT,
            content_rating VARCHAR(63),
            FOREIGN KEY (name) REFERENCES descriptions (name)
        )
    """)

    cur.execute("""
        CREATE TABLE info (
            name VARCHAR(255),
            version VARCHAR(63),
            size BIGINT,
            price MONEY,
            downloads BIGINT,
            total_ratings BIGINT,
            FOREIGN KEY (name) REFERENCES descriptions (name)
        )
    """)

    google_csv = "googleplaystore.csv"
    # ['App', 'Category', 'Rating', 'Reviews', 'Size', 'Installs', 
    # 'Type', 'Price', 'Content Rating', 'Genres', 'Last Updated', 
    # 'Current Ver', 'Android Ver']
    
    apple_csv = "AppleStore.csv"
    # ['', 'id', 'track_name', 'size_bytes', 'currency', 'price', 
    # 'rating_count_tot', 'rating_count_ver', 'user_rating', 
    # 'user_rating_ver', 'ver', 'cont_rating', 'prime_genre', 
    # 'sup_devices.num', 'ipadSc_urls.num', 'lang.num', 'vpp_lic']

    apple_desc_csv = "appleStore_description.csv"
    # ['id', 'track_name', 'size_bytes', 'app_desc']

    google_user_review_csv = "googleplaystore_user_reviews.csv"
    # ['App', 'Translated_Review', 'Sentiment', 'Sentiment_Polarity', 
    # 'Sentiment_Subjectivity']

    google_csv_len = sum(1 for line in open(google_csv, "r", encoding="utf8"))
    google_user_review_csv_len = sum(1 for line in open(google_user_review_csv, "r", encoding="utf8"))
    apple_csv_len = sum(1 for line in open(apple_csv, "r", encoding="utf8"))
    apple_desc_csv_len = sum(1 for line in open(apple_desc_csv, "r", encoding="utf8"))

    with open(google_csv, "r", encoding="utf8") as fd:
        l = google_csv_len
        printProgressBar(0, l, prefix = 'Progress:', suffix = 'Complete', length = 50)
        csv_data = csv.reader(fd)
        row_counter = 0
        next(csv_data)
        for row in csv_data:
            row_counter += 1
            cur.execute(
                "INSERT INTO descriptions VALUES (%s, NULL) ON CONFLICT DO NOTHING",
                [row[0]]
            )
            printProgressBar(row_counter, l, prefix = 'Progress:', suffix = 'Complete', length = 50)
        printProgressBar(l, l, prefix = 'Progress:', suffix = 'Complete', length = 50)
        print(f"{row_counter} rows inserted from Google to descriptions")
    
    with open(google_user_review_csv, "r", encoding="utf8") as fd:
        l = google_user_review_csv_len
        printProgressBar(0, l, prefix = 'Progress:', suffix = 'Complete', length = 50)
        csv_data = csv.reader(fd)
        next(csv_data)
        row_counter = 0
        for row in csv_data:
            row_counter += 1
            cur.execute(
                "INSERT INTO descriptions VALUES (%s, NULL) ON CONFLICT DO NOTHING",
                [row[0]]
            )
            printProgressBar(row_counter, l, prefix = 'Progress:', suffix = 'Complete', length = 50)
        printProgressBar(l, l, prefix = 'Progress:', suffix = 'Complete', length = 50)
        print(f"{row_counter} rows inserted from Google Reviews to descriptions")

    with open(apple_desc_csv, "r", encoding="utf8") as fd:
        l = apple_desc_csv_len
        printProgressBar(0, l, prefix = 'Progress:', suffix = 'Complete', length = 50)
        csv_data = csv.reader(fd)
        row_counter = 0
        next(csv_data)
        for row in csv_data:
            description = row[3]
            row_counter += 1
            cur.execute(
                "INSERT INTO descriptions VALUES (%s, %s) ON CONFLICT DO NOTHING",
                [row[1], description]
            )
            printProgressBar(row_counter, l, prefix = 'Progress:', suffix = 'Complete', length = 50)
        printProgressBar(l, l, prefix = 'Progress:', suffix = 'Complete', length = 50)
        print(f"{row_counter} rows inserted from Apple to descriptions")

    with open(google_csv, "r", encoding="utf8") as fd:
        l = google_csv_len
        printProgressBar(0, l, prefix = 'Progress:', suffix = 'Complete', length = 50)
        csv_data = csv.reader(fd)
        next(csv_data)
        row_counter = 0
        progress_counter = 0
        for row in csv_data:
            categories = set()
            categories.add(row[1].lower())
            genres = row[9].split(";")
            for genre in genres:
                genre = genre.replace("&", "and").replace(" ", "_").lower()
                categories.add(genre)
            for category in list(categories):
                row_counter += 1
                cur.execute(
                    "INSERT INTO apps VALUES (%s, %s) ON CONFLICT DO NOTHING",
                    [row[0], category]
                )
            progress_counter += 1
            printProgressBar(progress_counter, l, prefix = 'Progress:', suffix = 'Complete', length = 50)
        printProgressBar(l, l, prefix = 'Progress:', suffix = 'Complete', length = 50)
        print(f"{row_counter} rows inserted from Google to apps")
    
    # {'finance', 'business', 'book', 'medical', 'navigation', 
    # 'food_and_drink', 'health_and_fitness', 'sports', 'music', 
    # 'social_networking', 'games', 'travel', 'shopping', 'catalogs', 
    # 'weather', 'productivity', 'lifestyle', 'entertainment', 'news', 
    # 'education', 'reference', 'photo_and_video', 'utilities'}

    with open(apple_csv, "r", encoding="utf8") as fd:
        l = apple_csv_len
        printProgressBar(0, l, prefix = 'Progress:', suffix = 'Complete', length = 50)
        csv_data = csv.reader(fd)
        next(csv_data)
        row_counter = 0
        for row in csv_data:
            genre = row[12].lower().replace("&", "and").replace(" ", "_")
            row_counter += 1
            cur.execute(
                "INSERT INTO apps VALUES (%s, %s) ON CONFLICT DO NOTHING",
                [row[2], genre]
            )
            printProgressBar(row_counter, l, prefix = 'Progress:', suffix = 'Complete', length = 50)
        printProgressBar(l, l, prefix = 'Progress:', suffix = 'Complete', length = 50)
        print(f"{row_counter} rows inserted from Apple to apps")

    units = {"k": 10**3, "m": 10**6}
    with open(google_csv, "r", encoding="utf8") as fd:
        l = google_csv_len
        printProgressBar(0, l, prefix = 'Progress:', suffix = 'Complete', length = 50)
        csv_data = csv.reader(fd)
        next(csv_data)
        row_counter = 0
        for row in csv_data:
            row_counter += 1
            if "m" in row[4].lower():
                size = float(row[4].lower().rstrip("m")) * units["m"]
                size = str(int(size))
            elif "k" in row[4].lower():
                size = float(row[4].lower().rstrip("k")) * units["k"]
                size = str(int(size))
            else:
                size = None

            if "$" in row[7]:
                money = float(row[7].lstrip("$"))
            elif row[7] == '0':
                money = float(row[7])
            else:
                money = None

            if row[11] == "Varies with device" or row[11] == "NaN":
                 version = None
            else:
                 version = row[11]

            installs = row[5].rstrip("+").replace(",", "")
            try:
                installs = int(installs)
            except ValueError:
                installs = None

            cur.execute(
                "INSERT INTO info VALUES (%s, %s, %s, %s, %s, NULL) ON CONFLICT DO NOTHING",
                 [row[0], version, size, money, installs]
            )
            printProgressBar(row_counter, l, prefix = 'Progress:', suffix = 'Complete', length = 50)
        printProgressBar(l, l, prefix = 'Progress:', suffix = 'Complete', length = 50)
        print(f"{row_counter} rows inserted from Google to info")

    with open(apple_csv, "r", encoding="utf8") as fd:
        l = apple_csv_len
        printProgressBar(0, l, prefix = 'Progress:', suffix = 'Complete', length = 50)
        csv_data = csv.reader(fd)
        next(csv_data)
        row_counter = 0
        for row in csv_data:
            row_counter += 1
            cur.execute(
                "INSERT INTO info VALUES (%s, %s, %s, %s, NULL, %s) ON CONFLICT DO NOTHING",
                 [row[2], row[10], row[3], row[5], row[6]]
            )
            printProgressBar(row_counter, l, prefix = 'Progress:', suffix = 'Complete', length = 50)
        printProgressBar(l, l, prefix = 'Progress:', suffix = 'Complete', length = 50)
        print(f"{row_counter} rows inserted from Apple to info")

    with open(google_user_review_csv, "r", encoding="utf8") as fd:
        l = google_user_review_csv_len
        printProgressBar(0, l, prefix = 'Progress:', suffix = 'Complete', length = 50)
        csv_data = csv.reader(fd)
        next(csv_data)
        row_counter = 0
        for row in csv_data:
            row_counter += 1
            cur.execute(
                'INSERT INTO reviews VALUES (%s, %s) ON CONFLICT DO NOTHING',
                [row[0], row[1]]
            )
            printProgressBar(row_counter, l, prefix = 'Progress:', suffix = 'Complete', length = 50)
        printProgressBar(l, l, prefix = 'Progress:', suffix = 'Complete', length = 50)
        print(f"{row_counter} rows inserted from Google Reviews to reviews")

    with open(apple_csv, "r", encoding="utf8") as fd:
        l = apple_csv_len
        printProgressBar(0, l, prefix = 'Progress:', suffix = 'Complete', length = 50)
        csv_data = csv.reader(fd)
        row_counter = 0
        next(csv_data)
        for row in csv_data:
            row_counter += 1
            cur.execute(
                "INSERT INTO reviews VALUES (%s, NULL) ON CONFLICT DO NOTHING",
                [row[2]]
            )
            printProgressBar(row_counter, l, prefix = 'Progress:', suffix = 'Complete', length = 50)
        printProgressBar(l, l, prefix = 'Progress:', suffix = 'Complete', length = 50)
        print(f"{row_counter} rows inserted from Apple to reviews")

    with open(google_csv, "r", encoding="utf8") as fd:
        l = google_csv_len
        printProgressBar(0, l, prefix = 'Progress:', suffix = 'Complete', length = 50)
        csv_data = csv.reader(fd)
        next(csv_data)
        row_counter = 0
        for row in csv_data:
            row_counter += 1
            total = row[3].replace('M',"00000").replace('.',"")
            cur.execute(
                 'INSERT INTO ratings VALUES (%s, %s, %s, %s) ON CONFLICT DO NOTHING',
                 [row[0], row[2], total, row[8]]
            )
            printProgressBar(row_counter, l, prefix = 'Progress:', suffix = 'Complete', length = 50)
        printProgressBar(l, l, prefix = 'Progress:', suffix = 'Complete', length = 50)
        print(f"{row_counter} rows inserted from Google to ratings")

    with open(apple_csv, "r", encoding="utf8") as fd:
        l = apple_csv_len
        printProgressBar(0, l, prefix = 'Progress:', suffix = 'Complete', length = 50)
        csv_data = csv.reader(fd)
        next(csv_data)
        row_counter = 0
        for row in csv_data:
            row_counter += 1
            total = row[3].replace('M',"00000").replace('.',"")
            cur.execute(
                'INSERT INTO ratings VALUES(%s, %s, %s, %s) ON CONFLICT DO NOTHING',
                [row[2], row[8], row[6], row[10]]
            )
            printProgressBar(row_counter, l, prefix = 'Progress:', suffix = 'Complete', length = 50)
        printProgressBar(l, l, prefix = 'Progress:', suffix = 'Complete', length = 50)
        print(f"{row_counter} rows inserted from Apple to ratings")

if __name__ == "__main__":
    main()