import psycopg2


class AppData:

    def __init__(self, connection_string):
        self.conn = psycopg2.connect(connection_string)

    def check_connectivity(self):
        cursor = self.conn.cursor()
        cursor.execute("SELECT * FROM apps LIMIT 1")
        records = cursor.fetchall()
        return len(records) == 1

    def list_genres(self):
        cursor = self.conn.cursor()
        cursor.execute("""
            SELECT DISTINCT(genre) FROM apps
        """)
        records = cursor.fetchall()
        return records
    
    def list_apps(self):
        cursor = self.conn.cursor()
        cursor.execute("""
            SELECT name FROM apps
        """)
        records = cursor.fetchall()
        return records
    
    def get_top_ten_genre(self, genre):
        cursor = self.conn.cursor()
        cursor.execute("""
            SELECT apps.name, 
                AVG(info.downloads) AS downloads, 
                AVG(info.total_ratings) AS total_ratings, 
                AVG(ratings.rating) AS rating 
            FROM apps, info, ratings 
            WHERE apps.name=info.name 
                AND apps.name=ratings.name 
                AND apps.genre=%s 
            GROUP BY apps.name 
            ORDER BY downloads DESC, total_ratings DESC, rating DESC 
            LIMIT 10
        """, [genre])
        records = cursor.fetchall()
        return records

    def get_app_info(self, name):
        cursor = self.conn.cursor()
        cursor.execute("""SELECT * FROM info WHERE LOWER(info.name)=LOWER(%s);""", [name])
        record = cursor.fetchall()
        return record

    def get_apps_by_category(self, category):
        cursor = self.conn.cursor()
        cursor.execute("""SELECT apps.name FROM apps WHERE LOWER(apps.genre)= LOWER(%s);""", [category])
        record = cursor.fetchall()
        names = []
        for t in record:
            names.append(t[0])
        return names

    def list_app_rating_condition(self, rating):
        cursor = self.conn.cursor()
        cursor.execute("SELECT name, rating FROM ratings WHERE rating > %s ORDER BY rating", [rating])
        apps = cursor.fetchall()
        names = []
        for t in apps:
            names.append(t[0])
        return names

    def average_rating(self, category):
        cursor = self.conn.cursor()
        category = category.lower()
        cursor.execute("SELECT AVG(rating) FROM ratings, apps WHERE ratings.name = apps.name AND apps.genre = %s AND ratings.rating != 'NaN'", [category])
        rating = cursor.fetchall()
        #round the rating to 2 decimal places
        return round(rating[0][0],2)

    def get_top_ten_price_range(self,l,h):
        low = float(l)
        high = float(h)
        cursor = self.conn.cursor()
        cursor.execute("""
                    SELECT apps.name, 
                        AVG(info.downloads) AS downloads, 
                        AVG(info.total_ratings) AS total_ratings, 
                        AVG(ratings.rating) AS rating 
                    FROM apps, info, ratings 
                    WHERE apps.name=info.name 
                        AND info.price >= '%s'::float8::numeric::money
                        AND info.price <= '%s'::float8::numeric::money
                    GROUP BY apps.name 
                    ORDER BY downloads DESC, total_ratings DESC, rating DESC 
                    LIMIT 10
                """, [low,high])
        records = cursor.fetchall()
        return records
