import cmd
from app_data import AppData

preface = """
Welcome to the Application Database Shell!
This application is meant to combine information about mobile apps
found in Google and Apple App Stores. 

The data is stored using Postgres and describes an app's name, 
price, size, number of downloads, etc.

This data is under fair-use licenses from Kaggle.com.

In order to normalize this data, some incomplete columns were thrown 
out. The app sizes were normalized from ...k and ...m to *10**3 and 
*10**6. App prices were normalized too in order to be stored as 
Price objects. Given that both datasets saves title case of app 
names, names were kept in title case.

Type help or ? to list available commands
"""

class Shell(cmd.Cmd):
    intro = preface
    prompt = "shell >> "

    def __init__(self, service):
        super().__init__() 
        self.service = service

    def do_list_genres(self, arg):
        """This command lists all the genres each app is categorized in
           sytanx: list_genres <genre>
        """
        genres = [genre[0] for genre in self.service.list_genres()]
        genres.remove("february_11,_2018")
        genres.remove("1.9")
        for genre in genres:
            print(genre)
        return
    
    def complete_list_app_info(self, text, line, start_index, end_index):
        apps = [app[0] for app in self.service.list_apps()]
        if text:
            return [choice for choice in apps if choice.startswith(text)]
        else:
            return apps 

    def do_list_app_info(self, arg):
        """This provides all the information available about the chosen appication 
           syntax: list_app_info <app_name> """
        app = arg
        ret = self.service.get_app_info(app)
        infodict = {
            "APP": None,
            "VERSION": None,
            "SIZE": None,
            "PRICE": None,
            "DOWNLOADS": None,
            "TOTAL RATINGS": None
        }
        
        for found in ret:
            if infodict.get("APP") == None:
                infodict["APP"] = found[0]
            
            if infodict.get("VERSION") == None:
                infodict["VERSION"] = found[1]

            if infodict.get("SIZE") == None or infodict.get("SIZE") == 0:
                infodict["SIZE"] = found[2]
        
            if infodict.get("PRICE") == None:
                infodict["PRICE"] = found[3]
                
            if infodict.get("DOWNLOADS") == None or infodict.get("DOWNLOADS") ==0 :
                infodict["DOWNLOADS"] = found[4]
                    
            if infodict.get("TOTAL RATINGS") == None or infodict.get("TOTAL RATINGS") == 0:
                infodict["TOTAL RATINGS"] = found[5]
        
        print(f"{infodict.get('APP')} info:")       
        print(f"    version: {infodict.get('VERSION')}")
        print(f"    size: {infodict.get('SIZE')}")
        print(f"    price: {infodict.get('PRICE')}")
        print(f"    downloads: {infodict.get('DOWNLOADS')}")
        print(f"    total ratings: {infodict.get('TOTAL RATINGS')}")
        return
   
    def complete_get_apps_by_genre(self, text, line, start_index, end_index):
        genres = [genre[0] for genre in self.service.list_genres()]
        genres.remove("february_11,_2018")
        genres.remove("1.9")
        if text:
            return [choice for choice in genres if choice.startswith(text)]
        else:
            return genres 

    def do_get_apps_by_genre(self, arg):
        """This command lists all the apps in a particular genre 
           syntax: get_apps_by_genre <genre>"""
        names = self.service.get_apps_by_category(arg)
        for app in names:
            print(f"{app}")
        return
    
    def complete_top_ten_genres(self, text, line, start_index, end_index):
        genres = [genre[0] for genre in self.service.list_genres()]
        genres.remove("february_11,_2018")
        genres.remove("1.9")
        if text:
            return [choice for choice in genres if choice.startswith(text)]
        else:
            return genres

    def do_top_ten_genres(self, arg):
        """This command lists the top 10 most downloaded/rated apps of a genre
           syntax: top_ten_genres <genre>
        """
        if len(arg) == 0:
            print("Please choose a genre")
            return
        genre = arg
        top_ten_apps = [app[0] for app in self.service.get_top_ten_genre(genre)]
        print(f"Top Ten Apps in {genre}")
        for i in range(0, len(top_ten_apps)):
            print(f"    {i+1}. {top_ten_apps[i]}")

    def complete_average_rating(self, text, line, start_index, end_index):
        genres = [genre[0] for genre in self.service.list_genres()]
        genres.remove("february_11,_2018")
        genres.remove("1.9")
        if text:
            return [choice for choice in genres if choice.startswith(text)]
        else:
            return genres

    def do_average_rating(self, arg):
        """This command lists the average ratings of all apps of a genre
           syntax: average_rating <genre>
        """
        if len(arg) == 0:
            print("Please choose a category")
            return
        category = arg
        average_rating = self.service.average_rating(category)
        print(f"Average Rating for {category}: {average_rating}")

    def do_larger_ratings(self, arg):
        """This command lists all apps with ratings greater than indicated 
           syntax: larger_ratings <rating_num>
        """
        if len(arg) == 0:
            print("Please enter a rating")
            return
        smallest_rating = arg
        app_list = self.service.list_app_rating_condition(smallest_rating)
        print(f"Apps with a Rating > {smallest_rating}")
        for i in range(0, len(app_list)):
            print(f"    {i+1}. {app_list[i]}")

    def do_top_ten_price_range(self,arg):
        """This command lists all apps that are within a price range
           syntax: top_ten_price_range <smallest_price,largest_price>
        """
        if "," not in arg:
            print("Please type comma (,) separated input")
            return
        low, high = arg.split(',')
        top_ten_apps = [app[0] for app in self.service.get_top_ten_price_range(low,high)]
        print(f"Top Ten Apps between ${low} & ${high}")
        for i in range(0,len(top_ten_apps)):
            print(f"    {i + 1}. {top_ten_apps[i]}")

    def do_EOF(self, arg):
        print("Goodbye")
        return True


def main():
    connection_string = "host=localhost dbname=finalproject user=finalproject password=finalproject"
    service = AppData(connection_string)

    if service.check_connectivity() == True:
        print("Successfully Connected")
    
    try:
        Shell(service).cmdloop()
    except KeyboardInterrupt:
        print("Goodbye")


if __name__ == "__main__":
    main()
